﻿using System;
using System.Globalization;
using System.Linq;

namespace CamelCase_Pospisil
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Zadejte text, který chcete konvertovat na CamelCase: ");
            string zText = Console.ReadLine();
            Console.WriteLine();
            Console.WriteLine("Zde je překonvertovaný text do CamelCasu: ");
            Console.WriteLine(Camel(zText));

            Console.ReadLine();
        }
        static string Camel(string text)
        {
            TextInfo txtInfo = new CultureInfo("en-us", false).TextInfo;
            text = txtInfo.ToTitleCase(text).Replace(" ", String.Empty).Replace("-", String.Empty).Replace("_", String.Empty);
            text = $"{text.First().ToString().ToLowerInvariant()}" + $"{text.Substring(1)}";
            return text;
        }
    }
}

